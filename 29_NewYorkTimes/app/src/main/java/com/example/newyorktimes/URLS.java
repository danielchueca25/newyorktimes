package com.example.newyorktimes;

public interface URLS {
    String INTERNAL_START = "https://api.nytimes.com/svc/";
    String INTERNAL_KEY = "uRjwc7qOAD2OBlEhhva7GmKLNevxwpKs";

    String MOVIES = INTERNAL_START + "movies/v2/reviews/search.json?query=";
    // query del search
    String KEY_MOVIES = "&api-key=" + INTERNAL_KEY;

    String TOP_STORIES = INTERNAL_START + "topstories/v2/";
    // item spinner
    String KEY_TOP_STORIES = ".json?api-key=" + INTERNAL_KEY;
}
