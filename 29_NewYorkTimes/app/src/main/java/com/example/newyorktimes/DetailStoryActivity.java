package com.example.newyorktimes;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class DetailStoryActivity extends AppCompatActivity {
    private ImageView imgStoryDet;
    private TextView titleStoryDet, byStoryDet, sectionStoryDet, subsectionStoryDet, itemtypeStoryDet,
            dateStoryDet, abstractStoryDet;
    private Button btnUrlStoryDet;
    private String uriBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_story);

        // hook
        imgStoryDet = findViewById(R.id.imgStoryDet);
        titleStoryDet = findViewById(R.id.titleStoryDet);
        byStoryDet = findViewById(R.id.byStoryDet);
        sectionStoryDet = findViewById(R.id.sectionStoryDet);
        subsectionStoryDet = findViewById(R.id.subsectionStoryDet);
        itemtypeStoryDet = findViewById(R.id.itemtypeStoryDet);
        dateStoryDet = findViewById(R.id.dateStoryDet);
        abstractStoryDet = findViewById(R.id.abstractStoryDet);
        btnUrlStoryDet = findViewById(R.id.btnUrlStoryDet);

        // set
        if (getIntent().getStringExtra("src") == null)
            imgStoryDet.setImageResource(R.drawable.nytlogo1);
        else
            Picasso.get().load( getIntent().getStringExtra("src") )
                    .fit()
                    .centerCrop()
                    .into(imgStoryDet);

        if (getIntent().getStringExtra("subsection").isEmpty())
            subsectionStoryDet.setText("Without subsection");
        else
            subsectionStoryDet.setText(getIntent().getStringExtra("subsection"));

        titleStoryDet.setText(getIntent().getStringExtra("title"));
        byStoryDet.setText(getIntent().getStringExtra("by"));
        sectionStoryDet.setText(getIntent().getStringExtra("section"));
        itemtypeStoryDet.setText(getIntent().getStringExtra("item"));
        dateStoryDet.setText(getIntent().getStringExtra("date"));
        abstractStoryDet.setText(getIntent().getStringExtra("abstract"));
        uriBtn =  getIntent().getStringExtra("url");

        btnUrlStoryDet.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriBtn));
            startActivity(intent);
        });


    }
}