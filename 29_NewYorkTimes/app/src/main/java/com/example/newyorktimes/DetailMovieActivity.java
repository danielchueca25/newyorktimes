package com.example.newyorktimes;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class DetailMovieActivity extends AppCompatActivity {
    private ImageView imgMovieDet;
    private TextView titleMovieDet, byMovieDet, headlineMovieDet, dateMovieDet, summaryMovieDet;
    private Button btnUrlMovieDet;
    private String uriButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        // hook
        imgMovieDet = findViewById(R.id.imgMovieDet);
        titleMovieDet = findViewById(R.id.titleMovieDet);
        byMovieDet = findViewById(R.id.byMovieDet);
        headlineMovieDet = findViewById(R.id.headlineMovieDet);
        dateMovieDet = findViewById(R.id.dateMovieDet);
        summaryMovieDet = findViewById(R.id.summaryMovieDet);
        btnUrlMovieDet = findViewById(R.id.btnUrlMovieDet);

        // set
        if (getIntent().getStringExtra("src") == null)
            imgMovieDet.setImageResource(R.drawable.nytlogo1);
        else
            Picasso.get().load( getIntent().getStringExtra("src") )
                    .fit()
                    .centerCrop()
                    .into(imgMovieDet);

        titleMovieDet.setText( getIntent().getStringExtra("title") );
        byMovieDet.setText( getIntent().getStringExtra("by") );
        headlineMovieDet.setText( getIntent().getStringExtra("headline") );
        dateMovieDet.setText( getIntent().getStringExtra("date") );
        summaryMovieDet.setText( getIntent().getStringExtra("summary") );
        uriButton =  getIntent().getStringExtra("url");

        btnUrlMovieDet.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriButton));
            startActivity(intent);
        });
    }
}