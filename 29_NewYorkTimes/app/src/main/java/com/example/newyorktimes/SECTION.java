package com.example.newyorktimes;

public interface SECTION {
    String ARTS = "arts";
    String AUTOMOBILES = "automobiles";
    String BOOKS = "books";
    String BUSINESS = "business";
    String FASHION = "fashion";
    String FOOD = "food";
    String HEALTH = "health";
    String HOME = "home";
    String INSIDER = "insider";
    String MAGAZINE = "magazine";
    String MOVIES = "movies";
    String NYREGION = "nyregion";
    String OBITUARIES = "obituaries";
    String OPINION = "opinion";
    String POLITICS = "politics";
    String REALSTATE = "realestate";
    String SCIENCE = "science";
    String SPORTS = "sports";
    String SUNDAYREVIEW = "sundayreview";
    String TECHNOLOGY = "technology";
    String THEATER = "theater";
    String TMAGAZINE = "t-magazine";
    String TRAVEL = "travel";
    String UPSHOT = "upshot";
    String US = "us";
    String WORLD = "world";
}
