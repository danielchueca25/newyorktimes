package com.example.newyorktimes;

public class StoriesSpinner {
    private String textSpinner;

    public StoriesSpinner(String textSpinner) {
        this.textSpinner = textSpinner;
    }

    public String getTextSpinner() {
        return textSpinner;
    }

    public void setTextSpinner(String textSpinner) {
        this.textSpinner = textSpinner;
    }
}
