package com.example.newyorktimes;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private LinearLayout linearStatus, linearSearch, linearRecords;
    private RadioGroup radioGroup;
    private TextView searchText, statusNumText, recordsNumText, errorText;
    private RecyclerView recycler;
    private ImageView imgSearch;
    private EditText searchInput;
    private Spinner spinner;

    private StoriesAdapter storiesAdapter;
    private ArrayList<StoriesSpinner> storiesSpinnerArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // hook
        linearStatus = findViewById(R.id.linearStatus);
        linearSearch = findViewById(R.id.linearSearch);
        linearRecords = findViewById(R.id.linearRecords);
        radioGroup = findViewById(R.id.radioGroup);
        searchText = findViewById(R.id.searchText);
        statusNumText = findViewById(R.id.statusNumText);
        recordsNumText = findViewById(R.id.recordsNumText);
        errorText = findViewById(R.id.errorText);
        recycler = findViewById(R.id.recycler);
        imgSearch = findViewById(R.id.imgSearch);
        searchInput = findViewById(R.id.searchInput);
        spinner = findViewById(R.id.spinner);


        // -----------------------------
        // radio buttons
        radioGroup.setOnCheckedChangeListener((radioGroup, i) -> {
            int selected = radioGroup.getCheckedRadioButtonId();

            searchText.setVisibility(View.VISIBLE);
            linearStatus.setVisibility(View.INVISIBLE);
            linearRecords.setVisibility(View.INVISIBLE);
            recycler.setVisibility(View.INVISIBLE);
            searchInput.setText("");

            switch (selected) {
                case R.id.movieRB:
                    searchText.setText(R.string.text_search);
                    linearSearch.setVisibility(View.VISIBLE);
                    spinner.setVisibility(View.INVISIBLE);

                    imgSearch.setOnClickListener(view -> {
                        errorText.setVisibility(View.INVISIBLE);
                        recycler.setVisibility(View.INVISIBLE);
                        searchMovie(searchInput);
                    });
                    break;


                case R.id.storyRB:
                    searchText.setText(R.string.text_spinner);
                    linearSearch.setVisibility(View.INVISIBLE);
                    spinner.setVisibility(View.VISIBLE);

                    init_stories();
                    storiesAdapter = new StoriesAdapter(getApplicationContext(), storiesSpinnerArrayList);
                    spinner.setAdapter(storiesAdapter);

                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            StoriesSpinner selectedItem = (StoriesSpinner) parent.getItemAtPosition(position);
                            String selectedItemTextSpinner = selectedItem.getTextSpinner();

                            if (!selectedItemTextSpinner.equals("Select a section")) {
                                errorText.setVisibility(View.INVISIBLE);
                                recycler.setVisibility(View.INVISIBLE);
                                searchStories(selectedItemTextSpinner);
                            } else {
                                recycler.setVisibility(View.INVISIBLE);
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            Toast.makeText(MainActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
            }

        });

    }

    private void init_stories() {
        storiesSpinnerArrayList = new ArrayList<>();
        storiesSpinnerArrayList.add(new StoriesSpinner("Select a section"));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.ARTS));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.AUTOMOBILES));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.BOOKS));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.BUSINESS));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.FASHION));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.FOOD));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.HEALTH));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.HOME));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.INSIDER));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.MAGAZINE));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.MOVIES));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.NYREGION));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.OBITUARIES));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.OPINION));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.POLITICS));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.REALSTATE));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.SCIENCE));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.SPORTS));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.SUNDAYREVIEW));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.TECHNOLOGY));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.THEATER));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.TMAGAZINE));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.TRAVEL));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.UPSHOT));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.US));
        storiesSpinnerArrayList.add(new StoriesSpinner(SECTION.WORLD));
    }

    private void searchMovie(EditText searchInput) {
        String searchVar = searchInput.getText().toString().trim();
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URLS.MOVIES + searchVar + URLS.KEY_MOVIES,
                null,
                (Response.Listener<JSONObject>) response -> {
                    String num_results = response.optString("num_results");
                    List<Movies_result> results = new ArrayList<>();

                    if (Integer.parseInt(num_results) > 0) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");

                            statusNumText.setText("200");
                            recordsNumText.setText(num_results);

                            results = Arrays.asList(
                                    new GsonBuilder().create().fromJson(jsonArray.toString(),
                                            Movies_result[].class));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        linearStatus.setVisibility(View.VISIBLE);
                        linearRecords.setVisibility(View.VISIBLE);
                        recycler.setVisibility(View.VISIBLE);
                        recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        CustomAdapter_Movies adapter = new CustomAdapter_Movies(results, getApplicationContext());
                        recycler.setAdapter(adapter);

                    } else {
                        // poner franceaaa para comprobar
                        errorText.setVisibility(View.VISIBLE);
                        statusNumText.setText("400");
                        recordsNumText.setText("0");
                    }

                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

    private void searchStories(String sectionSpinner) {
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URLS.TOP_STORIES + sectionSpinner + URLS.KEY_TOP_STORIES,
                null,
                (Response.Listener<JSONObject>) response -> {
                    String num_results = response.optString("num_results");
                    List<Stories_result> results = new ArrayList<>();

                    try {
                        JSONArray jsonArray = response.getJSONArray("results");

                        statusNumText.setText("200");
                        recordsNumText.setText(num_results);

                        results = Arrays.asList(
                                new GsonBuilder().create().fromJson(jsonArray.toString(),
                                        Stories_result[].class));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    linearStatus.setVisibility(View.VISIBLE);
                    linearRecords.setVisibility(View.VISIBLE);
                    recycler.setVisibility(View.VISIBLE);
                    recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    CustomAdapter_Stories adapter = new CustomAdapter_Stories(results, getApplicationContext());
                    recycler.setAdapter(adapter);

                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }
}