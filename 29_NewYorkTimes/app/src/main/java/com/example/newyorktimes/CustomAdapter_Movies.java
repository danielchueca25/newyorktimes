package com.example.newyorktimes;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomAdapter_Movies extends RecyclerView.Adapter<CustomAdapter_Movies.MyViewHolder>{
    private List<Movies_result> movies;
    private Context context;

    public CustomAdapter_Movies(List<Movies_result> movies, Context context) {
        this.movies = movies;
        this.context = context;
    }

    // ----------------------------------------------------------------------------------

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.data_movies, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.titleMovieText.setText(movies.get(position).getDisplay_title());
        holder.autorMovieText.setText(movies.get(position).getByline());
        holder.headlineMovieText.setText(movies.get(position).getHeadline());
        holder.dateMovieText.setText(movies.get(position).getPublication_date());

        if (movies.get(position).getMultimedia() == null)
            holder.imgMovie.setImageResource(R.drawable.nytlogo1);
        else
            Picasso.get().load(movies.get(position).getMultimedia().getSrc())
                    .fit()
                    .centerCrop()
                    .into(holder.imgMovie);

        holder.relativeRowMovie.setOnClickListener(view -> {
            Intent intent = new Intent(context, DetailMovieActivity.class);
            if (movies.get(position).getMultimedia() == null)
                intent.putExtra("src", "null");
            else
                intent.putExtra("src", movies.get(position).getMultimedia().getSrc());
            intent.putExtra("title", movies.get(position).getDisplay_title());
            intent.putExtra("by", movies.get(position).getByline());
            intent.putExtra("headline", movies.get(position).getHeadline());
            intent.putExtra("date", movies.get(position).getPublication_date());
            intent.putExtra("summary", movies.get(position).getSummary_short());
            intent.putExtra("url", movies.get(position).getLink().getUrl());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    // ----------------------------------------------------------------------------------

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeRowMovie;
        private ImageView imgMovie;
        private TextView titleMovieText, autorMovieText, headlineMovieText, dateMovieText;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgMovie = itemView.findViewById(R.id.imgMovie);
            titleMovieText = itemView.findViewById(R.id.titleMovieText);
            autorMovieText = itemView.findViewById(R.id.autorMovieText);
            headlineMovieText = itemView.findViewById(R.id.headlineMovieText);
            dateMovieText = itemView.findViewById(R.id.dateMovieText);

            relativeRowMovie = itemView.findViewById(R.id.relativeRowMovie);
        }
    }
}
