package com.example.newyorktimes;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomAdapter_Stories extends RecyclerView.Adapter<CustomAdapter_Stories.MyViewHolder>{
    private List<Stories_result> stories;
    private Context context;

    public CustomAdapter_Stories(List<Stories_result> stories, Context context) {
        this.stories = stories;
        this.context = context;
    }

    // ----------------------------------------------------------------------------------

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.data_stories, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.titleStoryText.setText(stories.get(position).getTitle());
        holder.itemTypeStoryText.setText(stories.get(position).getItem_type());
        holder.autorStoryText.setText(stories.get(position).getByline());

        if (stories.get(position).getMultimedia() == null)
            holder.imgStory.setImageResource(R.drawable.nytlogo1);
        else
            Picasso.get().load(stories.get(position).getMultimedia().get(1).getUrl())
                    .fit()
                    .centerCrop()
                    .into(holder.imgStory);

        if (stories.get(position).getSubsection().isEmpty())
            holder.sectionStoryText.setText("Without subsection");
        else
            holder.sectionStoryText.setText(stories.get(position).getSubsection());

        holder.relativeRowStory.setOnClickListener(view -> {
            Intent intent = new Intent(context, DetailStoryActivity.class);

            intent.putExtra("src", stories.get(position).getMultimedia().get(1).getUrl());
            intent.putExtra("title", stories.get(position).getTitle());
            intent.putExtra("by", stories.get(position).getByline());
            intent.putExtra("section", stories.get(position).getSection());
            intent.putExtra("subsection", stories.get(position).getSubsection());
            intent.putExtra("item", stories.get(position).getItem_type());
            intent.putExtra("date", stories.get(position).getCreated_date());
            intent.putExtra("abstract", stories.get(position).getMyabstract());
            intent.putExtra("url", stories.get(position).getUrl());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return stories.size();
    }

    // ----------------------------------------------------------------------------------

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeRowStory;
        private ImageView imgStory;
        private TextView titleStoryText, sectionStoryText, itemTypeStoryText, autorStoryText;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgStory = itemView.findViewById(R.id.imgStory);
            titleStoryText = itemView.findViewById(R.id.titleStoryText);
            sectionStoryText = itemView.findViewById(R.id.sectionStoryText);
            itemTypeStoryText = itemView.findViewById(R.id.itemTypeStoryText);
            autorStoryText = itemView.findViewById(R.id.autorStoryText);

            relativeRowStory = itemView.findViewById(R.id.relativeRowStory);
        }
    }
}
